#include <iostream>
#include <fstream>
#include <sstream>
#include <ctype.h>

using namespace std;

/// \brief ImportText import the text for encryption
/// \param inputFilePath: the input file path
/// \param text: the resulting text
/// \return the result of the operation, true is success, false is error
bool ImportText(const string& inputFilePath,
                string& text);

/// \brief Encrypt encrypt the text
/// \param text: the text to encrypt
/// \param password: the password for encryption
/// \param encryptedText: the resulting encrypted text
/// \return the result of the operation, true is success, false is error
bool Encrypt(const string& text,
             const string& password,
             string& encryptedText);

/// \brief Decrypt decrypt the text
/// \param text: the text to decrypt
/// \param password: the password for decryption
/// \param decryptedText: the resulting decrypted text
/// \return the result of the operation, true is success, false is error
bool Decrypt(const string& text,
             const string& password,
             string& decryptedText);

int main(int argc, char** argv)
{
  // a password for the encryption must be passed from commmand line
  if (argc < 2)
  {
    cerr<< "Password shall passed to the program"<< endl;
    return -1;
  }
  string password = argv[1];

  // name of the file where there is the text to encrypt
  string inputFileName = "./text.txt";
  string text{};
  if (!ImportText(inputFileName, text))
  {
    cerr<< "Something goes wrong with import"<< endl;
    return -1;
  }
  else
    cout<< "Import successful: result= "<< text<< endl;

  string encryptedText{};
  if (!Encrypt(text, password, encryptedText))
  {
    cerr<< "Something goes wrong with encryption"<< endl;
    return -1;
  }
  else
    cout<< "Encryption successful: result= "<< encryptedText<< endl;

  string decryptedText{};
  // check if initial text and decrypted text are equal
  if (!Decrypt(encryptedText, password, decryptedText) || text != decryptedText)
  {
    cerr<< "Something goes wrong with decryption"<< endl;
    return -1;
  }
  else
    cout<< "Decryption successful: result= "<< decryptedText<< endl;

  return 0;
}

bool ImportText(const string& inputFilePath,
                string& text)
{
    ifstream file{ inputFilePath };

    if (!file)
        return false;

    getline(file, text);
    return true;
}

bool Encrypt(const string& text,
             const string& password,
             string& encryptedText)
{
    int c = 0;
    for (unsigned long long i = 0; i < text.size(); ++i)
    {
        c = text[i] + (toupper(password[i % password.length()]) % ('A'-1)); // aggiungo la pass modulandola per 'A'-1 cosi 'A'=1, 'B'=2 etc

        if (c > 'z')
            c = ' ' + (c - 'z') - 1;    // perche 'z'-c > 0 per ogni c e con il -1 puo uscire pure ' '

        encryptedText += c;     //il cast a char è automatico
    }

    return true;
}

bool Decrypt(const string& text,
             const string& password,
             string& decryptedText)
{
    int c{};
    for (unsigned long long i{}; i < text.size(); ++i)
    {
        c = text[i] - (toupper(password[i % password.length()]) % ('A'-1));

        if (c < ' ')
            c = 'z' - (' ' - c) + 1;    // perche ' '-c > 0 per ogni c e con il +1 puo uscire pure 'z'

        decryptedText += c;     // il cast a char è automatico
    }

    return true;
}

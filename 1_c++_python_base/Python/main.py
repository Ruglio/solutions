import sys


# \brief ImportText import the text for encryption
# \param inputFilePath: the input file path
# \return the result of the operation, true is success, false is error
# \return text: the resulting text
def importText(inputFilePath):
    file = open(inputFilePath, 'r')

    if not file:
        return False, ""

    text = file.readline()

    return True, text


# \brief Encrypt encrypt the text
# \param text: the text to encrypt
# \param password: the password for encryption
# \return the result of the operation, true is success, false is error
# \return encryptedText: the resulting encrypted text
def encrypt(text, password):
    encryptedText: str = ""

    # ord(' ') = 32, ord('z') = 122 vengono considerati range perché oltre alcuni caratteri non vengono stampati

    for i in range(len(text)):
        character = ord(text[i]) + (ord(password[i % len(password)].upper()) % (ord('A')-1))

        if character > ord('z'):
            character = ord(' ') + (character - ord('z')) - 1   # perche altrimenti lo spazio non uscirebbe mai, char % ord('z') > 1 per ogni char

        encryptedText += chr(character)

    return True, encryptedText


# \brief Decrypt decrypt the text
# \param text: the text to decrypt
# \param password: the password for decryption
# \return the result of the operation, true is success, false is error
# \return decryptedText: the resulting decrypted text
def decrypt(text, password):
    decryptedText: str = ""
    for i in range(len(text)):
        character = ord(text[i]) - (ord(password[i % len(password)].upper()) % (ord('A') - 1))

        if character < ord(' '):
            character = ord('z') - (ord(' ') - character) + 1   # cosi puo uscire 'z', ord(' ') % char > 0 per ogni char

        decryptedText += chr(character)

    return True, decryptedText


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Password shall passed to the program")
        exit(-1)
    password = sys.argv[1]

    inputFileName = "text.txt"

    [resultImport, text] = importText(inputFileName)
    if not resultImport:
        print("Something goes wrong with import")
        exit(-1)
    else:
        print("Import successful: text=", text)

    [resultEncrypt, encryptedText] = encrypt(text, password)
    if not resultEncrypt:
        print("Something goes wrong with encryption")
        exit(-1)
    else:
        print("Encryption successful: result= ", encryptedText)

    [resultEncrypt, decryptedText] = decrypt(encryptedText, password)
    if not resultEncrypt or text != decryptedText:
        print("Something goes wrong with decryption")
        exit(-1)
    else:
        print("Decryption successful: result= ", decryptedText)

#include "shape.h"
#include <math.h>


// MACRO instead of function to reduce computational cost
#define SQUARE(X) (X)*(X)

namespace ShapeLibrary {

Point::Point(const double &x, const double &y)
    {
        X = x;
        Y = y;
    }

    Point::Point(const Point &point)
    {
        X = point.X;
        Y = point.Y;
    }

    Ellipse::Ellipse(const Point &center, const int &a, const int &b)
    {
        _center = center;
        _a = a;
        _b = b;
    }

    double Ellipse::Area() const
    {
        return M_PI * _a * _b;
    }

    Circle::Circle(const Point &center, const int &radius)
        : Ellipse( center, radius, radius )
    {
    }

    Triangle::Triangle(const Point &p1, const Point &p2, const Point &p3)
        : _p1{ p1 }, _p2{ p2 }, _p3{ p3 }
    {
    }

    double Triangle::Area() const
    {
        return abs(_p1.X*(_p2-_p3).Y + _p2.X*(_p3-_p1).Y + _p3.X*(_p1-_p2).Y) / 2;
    }

    // TriangleEquiateral heredits from IPolygon, not triangle
    TriangleEquilateral::TriangleEquilateral(const Point &p1, const int &edge)
    {
        _p1 = p1;
        _edge = edge;
    }

    double TriangleEquilateral::Area() const
    {
        return (sqrt(3) / 4) * SQUARE(_edge);
    }

    Quadrilateral::Quadrilateral(const Point &p1, const Point &p2, const Point &p3, const Point &p4)
        : _p1{ p1 }, _p2{ p2 }, _p3{ p3 }, _p4{ p4 }
    {
    }

    double Quadrilateral::Area() const
    {
        return abs(_p1.X*_p2.Y + _p2.X*_p3.Y + _p3.X*_p4.Y + _p4.X*_p1.Y - _p2.X*_p1.Y - _p3.X*_p2.Y - _p4.X*_p3.Y - _p1.X*_p4.Y) / 2;
    }

    Parallelogram::Parallelogram(const Point &p1, const Point &p2, const Point &p4)
        : Quadrilateral( p1, p2, Point(p2 + p4 - p1), p4)
    {
    }

    // Rectangle heredits from IPolygon, not from Parallelogram or Quadrilateral
    Rectangle::Rectangle(const Point &p1, const int &base, const int &height)
    {
        _p1 = p1;
        _base = base;
        _height = height;
    }

    double Rectangle::Area() const
    {
        return _base * _height;
    }

    Square::Square(const Point &p1, const int &edge)
        : Rectangle(p1, edge, edge)
    {
    }

}

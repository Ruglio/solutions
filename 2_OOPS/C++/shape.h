#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>

using namespace std;

namespace ShapeLibrary {

  class Point
  {
    public:
      double X;
      double Y;

    public:
      Point(const double& x = 0,
            const double& y = 0);
      Point(const Point& point);

      Point operator+ (const Point& p) const
      {
          return Point(X + p.X, Y + p.Y);
      }
      Point operator- (const Point& p) const
      {
          return Point(X - p.X, Y - p.Y);
      }
  };

  class IPolygon
  {
    public:
      virtual double Area() const = 0;
  };

  class Ellipse : public IPolygon
  {
    public:
      Point _center;
      int _a;
      int _b;

    public:
      Ellipse(const Point& center,
              const int& a,
              const int& b);

      double Area() const;
  };

  // circle is special case of ellipse
  class Circle : public Ellipse
  {
    public:
      Circle(const Point& center,
             const int& radius);
  };


  class Triangle : public IPolygon
  {
    public:
      Point _p1;
      Point _p2;
      Point _p3;

    public:
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);

      double Area() const;
  };

  // bacause it is easier to get area directly
  // instead that getting the 3 points of triangle
  class TriangleEquilateral : public IPolygon
  {
    public:
      Point _p1;
      int _edge;

    public:
      TriangleEquilateral(const Point& p1,
                          const int& edge);

      double Area() const;
  };

  class Quadrilateral : public IPolygon
  {
    public:
      Point _p1;
      Point _p2;
      Point _p3;
      Point _p4;

    public:
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);

      double Area() const;
  };


  class Parallelogram : public Quadrilateral
  {
    public:
      Parallelogram(const Point& p1,
                    const Point& p2,
                    const Point& p4);
  };

  // bacause it is easier to get area directly
  // instead that passing through Quadrilateral
  class Rectangle : public IPolygon
  {
    public:
      Point _p1;
      int _base;
      int _height;

    public:
      Rectangle(const Point& p1,
                const int& base,
                const int& height);

      double Area() const;
  };

  class Square: public Rectangle
  {
    public:
      Square(const Point& p1,
             const int& edge);
  };
}

#endif // SHAPE_H

import math


class Point:
    def __init__(self, x: float, y: float):
        self.X = x
        self.Y = y

    def __add__(self, other):
        return Point(self.X + other.X, self.Y + other.Y)
    
    def __sub__(self, other):
        return Point(self.X - other.X, self.Y - other.Y)


class IPolygon:
    def area(self) -> float:
        pass


class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int):
        self.center = center
        self.a = a
        self.b = b

    def area(self) -> float:
        return math.pi * self.a * self.b


class Circle(Ellipse):
    def __init__(self, center: Point, radius: int):
        super().__init__(center, radius, radius)    # super for Ellipse constructor
        # Ellipse.__init__(self, center, radius, radius)


class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3

    def area(self) -> float:
        return math.fabs(self.p1.X*(self.p2-self.p3).Y+self.p2.X*(self.p3-self.p1).Y+self.p3.X*(self.p1-self.p2).Y) / 2


class TriangleEquilateral(IPolygon):
    def __init__(self, p1: Point, edge: int):
        self.p1 = p1
        self.edge = edge

    def area(self) -> float:
        return (math.sqrt(3) / 4) * math.pow(self.edge, 2)


class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3
        self.p4 = p4

    def area(self) -> float:
        return (self.p1.X*self.p2.Y + self.p2.X*self.p3.Y + self.p3.X*self.p4.Y + self.p4.X*self.p1.Y -
                self.p2.X*self.p1.Y - self.p3.X*self.p2.Y - self.p4.X*self.p3.Y - self.p1.X*self.p4.Y) / 2


class Parallelogram(Quadrilateral):
    def __init__(self, p1: Point, p2: Point, p4: Point):
        super().__init__(p1, p2, p2 + p4 - p1, p4)


class Rectangle(IPolygon):
    def __init__(self, p1: Point, base: int, height: int):
        self.p1 = p1
        self.base = base
        self.height = height

    def area(self) -> float:
        return self.base * self.height


class Square(Rectangle):
    def __init__(self, p1: Point, edge: int):
        super().__init__(p1, edge, edge)
        # Rectangle.__init__(self, p1, edge, edge)

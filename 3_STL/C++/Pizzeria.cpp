#include "Pizzeria.h"

namespace PizzeriaLibrary {

    Ingredient::Ingredient(const string &name, const string &description, const int price)
    {
        Name = name;
        Description = description;
        Price = price;
    }

    Pizza::Pizza(const string &name, const vector<Ingredient> ingredients)
    {
        Name = name;
        Ingredients = ingredients;
    }

    int Pizza::ComputePrice() const
    {
        // get total price of the pizza summing ingredient prices

        int pizzaPrice = 0;
        for(vector<Ingredient>::const_iterator it = Ingredients.begin(); it != Ingredients.end(); it++)
            pizzaPrice += it->Price;

        return pizzaPrice;
    }

    Order::Order(const vector<Pizza> pizzas, const int numOrder)
    {
        Pizzas = pizzas;
        NumOrder = numOrder;
    }

    const Pizza &Order::GetPizza(const int &position) const
    {        
        int numPizzas = Pizzas.size();
        if (position < 1 || position > numPizzas)
            throw runtime_error("Position passed is wrong");
        else
            return Pizzas[position - 1];
    }

    int Order::ComputeTotal() const
    {
        // get total order cost summing pizza prices

        int total = 0;
        for(unsigned int i = 0; i < Pizzas.size(); i++)
            total += Pizzas[i].ComputePrice();

        return total;
    }

    void Pizzeria::AddIngredient(const string &name, const string &description, const int &price)
    {
        // add an ingredient to the pizza if it is not present yet

        if(mapNameToIngredient.find(name) != mapNameToIngredient.end())
           throw runtime_error("Ingredient already inserted");

        Ingredient ingredient = Ingredient(name, description, price);
        mapNameToIngredient[name] = ingredient;
        // it is the same thing as
        // mapNameToIngredient.insert(pair<string, Ingredient>(name, ingredient));

    }

    const Ingredient &Pizzeria::FindIngredient(const string &name) const
    {
        // find an ingredient in the pizzeria and return it if it is present

        multimap<string, Ingredient>::const_iterator it = mapNameToIngredient.find(name);
        if(it != mapNameToIngredient.end())
           return it->second;

        else
           throw runtime_error("Ingredient not found");
    }

    void Pizzeria::AddPizza(const string &name, const vector<string> &ingredients)
    {
        // add a pizza in the pizzeria if it is not present yet

        if(mapNameToPizza.find(name) != mapNameToPizza.end())
            throw runtime_error("Pizza already inserted");

        // from a vector<string> of ingredients name create a vector<Ingredient>
        vector<Ingredient> vectorIngredients;
        for(unsigned int i = 0;  i < ingredients.size(); i++)
        {
            const Ingredient &ingredient= FindIngredient(ingredients[i]);
            vectorIngredients.push_back(ingredient);
        }

        Pizza pizza = Pizza(name, vectorIngredients);
        mapNameToPizza[name] = pizza;
    }

    const Pizza &Pizzeria::FindPizza(const string &name) const
    {
        // find a pizza and return an exception if it is not present

        auto it = mapNameToPizza.find(name);
        if(it != mapNameToPizza.end())
            return it->second;

        else
            throw runtime_error("Pizza not found");
    }

    int Pizzeria::CreateOrder(const vector<string> &pizzas)
    {
        // create an order and return its NumOrder attribute

        if(pizzas.empty())
            throw runtime_error("Empty order");
        else
        {
            vector<Pizza> vectorPizzas;
            for (unsigned int i = 0; i < pizzas.size(); i++)
            {
                Pizza pizza = FindPizza(pizzas[i]);
                vectorPizzas.push_back(pizza);
            }
            // numOrder starts from 1000
            int numOrder = 1000 + Orders.size();
            Order order = Order(vectorPizzas, numOrder);
            Orders.push_back(order);

            return numOrder;
        }
    }

    const Order &Pizzeria::FindOrder(const int &numOrder) const
    {
        // given numOrder find the respective Order

        int maxOrders = Orders.size();
        int orderPosition = numOrder - 1000;    // because orders starts from 1000
        if (orderPosition < 0 || orderPosition >= maxOrders)
            throw runtime_error("Order not found");
        else
            return Orders[orderPosition];
    }

    string Pizzeria::GetReceipt(const int &numOrder) const
    {
        // print the order, so name and price of each pizza

        const Order order = FindOrder(numOrder);
        int total = order.ComputeTotal();

        string receipt = "";
        // getPizza orders pizza from 1, not 0
        for (int i = 1;  i <= order.NumPizzas(); i++)
        {
            const Pizza &pizza = order.GetPizza(i);
            receipt += "- " + pizza.Name + ", " +to_string(pizza.ComputePrice()) + " euro" + "\n" ;
        }
        receipt += "  TOTAL: " + to_string(total)+ " euro" + "\n";

        return receipt;
    }

    string Pizzeria::ListIngredients() const
    {
        // map is ordered by key so we don t have to sort it

        string s = "";
        for(auto it = mapNameToIngredient.begin(); it != mapNameToIngredient.end(); it++)
        {
            s += it->second.Name + " - '"  + it->second.Description + "': " + to_string(it->second.Price)+ " euro" + "\n";
        }
        return s;
    }

    string Pizzeria::Menu() const
    {

        string s = "";
        for(auto it = mapNameToPizza.begin(); it != mapNameToPizza.end(); it++)
        {
            s += it->second.Name + " ("  + to_string(it->second.NumIngredients()) + " ingredients): "
                 + to_string(it->second.ComputePrice())+ " euro" + "\n";
        }
        return s;
    }

}

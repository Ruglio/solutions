#ifndef PIZZERIA_H
#define PIZZERIA_H

#include <iostream>
#include <vector>
#include <map>

using namespace std;

namespace PizzeriaLibrary {

  class Ingredient {
    public:
      string Name;
      int Price;
      string Description;

    public:
      // define the implicit default constructor even if other constructors are present
      Ingredient() = default;
      Ingredient(const string& name, const string& description, const int price);
  };

  class Pizza {
    public:
      vector<Ingredient> Ingredients;
      string Name;

    public:
      // define the implicit default constructor even if other constructors are present
      Pizza() = default;
      Pizza(const string& name, const vector<Ingredient> ingredients);

      void AddIngredient(const Ingredient& ingredient) { Ingredients.push_back(ingredient); }
      int NumIngredients() const { return Ingredients.size(); }
      int ComputePrice() const;
  };

  class Order {
    protected:
      vector<Pizza> Pizzas;
      int NumOrder;

    public:
      Order(const vector<Pizza> pizzas, const int numOrder);

      void InitializeOrder(int numPizzas) { Pizzas.reserve(numPizzas); }
      void AddPizza(const Pizza& pizza) { Pizzas.push_back(pizza); }
      const Pizza& GetPizza(const int& position) const;
      int NumPizzas() const { return Pizzas.size(); }
      int ComputeTotal() const;
  };

  class Pizzeria {
    private:
      map<string, Ingredient> mapNameToIngredient;
      map<string, Pizza> mapNameToPizza;
      vector<Order> Orders;

    public:
      void AddIngredient(const string& name, const string& description, const int& price);
      const Ingredient& FindIngredient(const string& name) const;
      void AddPizza(const string& name, const vector<string>& ingredients);
      const Pizza& FindPizza(const string& name) const;
      int CreateOrder(const vector<string>& pizzas);
      const Order& FindOrder(const int& numOrder) const;
      string GetReceipt(const int& numOrder) const;
      string ListIngredients() const;
      string Menu() const;
  };
};

#endif // PIZZERIA_H

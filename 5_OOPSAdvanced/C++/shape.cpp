#include "shape.h"
#include <math.h>


// MACRO instead of a function to minimize computational cost
#define SQUARE(X)  (X)*(X)

namespace ShapeLibrary {

  Ellipse::Ellipse()
  {
      _center = Point();
      _a = 0.0;
      _b = 0.0;
  }

  Ellipse::Ellipse(const Point &center, const double &a, const double &b)
  {
      _center = center;
      _a = a;
      _b = b;
  }

  double Ellipse::Perimeter() const
  {
      return 2 * M_PI * sqrt( (SQUARE(_a) + SQUARE(_b)) / 2 );
  }

  // Circle heredits from Ellipse
  Circle::Circle(const Point &center, const double &radius)
      : Ellipse(center, radius, radius)
  {
  }

  Triangle::Triangle(const Point& p1, const Point& p2, const Point& p3)
  {
      points.resize(3);
      points[0] = p1;
      points[1] = p2;
      points[2] = p3;
  }

  double Triangle::Perimeter() const
  {
      double l01 = (points[0] - points[1]).ComputeNorm2();
      double l12 = (points[1] - points[2]).ComputeNorm2();
      double l20 = (points[2] - points[0]).ComputeNorm2();
      double perimeter = l01 + l12 + l20;

      return perimeter;
  }

  TriangleEquilateral::TriangleEquilateral(const Point& p1, const double& edge)
  {
      Point p2 = p1 + Point(edge, 0.0);
      Point p3 = p1 + Point(edge / 2, (sqrt(3) / 2) * edge);

      points.resize(3);
      points[0] = p1;
      points[1] = p2;
      points[2] = p3;
  }



  Quadrilateral::Quadrilateral(const Point& p1, const Point& p2, const Point& p3, const Point& p4)
  {
      points.resize(4);
      points[0] = p1;
      points[1] = p2;
      points[2] = p3;
      points[3] = p4;
  }

  double Quadrilateral::Perimeter() const
  {
      double l01 = (points[0] - points[1]).ComputeNorm2();
      double l12 = (points[1] - points[2]).ComputeNorm2();
      double l23 = (points[2] - points[3]).ComputeNorm2();
      double l30 = (points[3] - points[0]).ComputeNorm2();
      double perimeter = l01 + l12 + l23 + l30;

      return perimeter;
  }

  Rectangle::Rectangle(const Point& p1, const Point& p2, const Point& p3, const Point& p4)
      : Quadrilateral(p1, p2, p3, p4)
  {
  }

  Rectangle::Rectangle(const Point& p1, const double& base, const double& height)
  {
      Point p2 = p1 + Point(0.0, base);
      Point p3 = p1 + Point(height, base);
      Point p4 = p1 + Point(height, 0.0);

      points.resize(4);
      points[0] = p1;
      points[1] = p2;
      points[2] = p3;
      points[3] = p4;
  }

  Square::Square(const Point& p1, const Point& p2, const Point& p3, const Point& p4)
      : Rectangle(p1, p2, p3, p4)
  {
  }

  Square::Square(const Point& p1, const double& edge)
      : Rectangle(p1, edge, edge)
  {
  }

  Point::Point(const double &x, const double &y)
  {
      X = x;
      Y = y;
  }

  double Point::ComputeNorm2() const
  {
      return sqrt(SQUARE(X) + SQUARE(Y));
  }

  Point Point::operator+(const Point& point) const
  {
      return Point(X + point.X, Y + point.Y);
  }

  Point Point::operator-(const Point& point) const
  {
      return Point(X - point.X, Y - point.Y);
  }

  Point&Point::operator-=(const Point& point)
  {
      X -= point.X;
      Y -= point.Y;
      return *this;
  }

  Point&Point::operator+=(const Point& point)
  {
      X += point.X;
      Y += point.Y;
      return *this;
  }


}

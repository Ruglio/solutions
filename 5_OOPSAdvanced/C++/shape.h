#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include <vector>
#include <math.h>
using namespace std;

namespace ShapeLibrary {

  class Point {
    public:
      double X;
      double Y;

      Point() { X = 0.0; Y = 0.0; }
      Point(const double& x, const double& y);

      double ComputeNorm2() const;

      Point operator+(const Point& point) const;
      Point operator-(const Point& point) const;
      Point& operator-=(const Point& point);
      Point& operator+=(const Point& point);
      friend ostream& operator<<(ostream& stream, const Point& point)
      {
        stream << "Point x = " << point.X << " y = " << point.Y << endl;
        return stream;
      }
  };

  class IPolygon {
    public:
      virtual double Perimeter() const = 0;
      virtual void AddVertex(const Point& point) = 0;

      friend inline bool operator< (const IPolygon& lhs, const IPolygon& rhs){ return lhs.Perimeter() < rhs.Perimeter(); }
      friend inline bool operator> (const IPolygon& lhs, const IPolygon& rhs){ return lhs.Perimeter() > rhs.Perimeter(); }
      friend inline bool operator<=(const IPolygon& lhs, const IPolygon& rhs){ return lhs.Perimeter() <= rhs.Perimeter(); }
      friend inline bool operator>=(const IPolygon& lhs, const IPolygon& rhs){ return lhs.Perimeter() >= rhs.Perimeter(); }

  };

  class Ellipse : public IPolygon
  {
    protected:
      Point _center;
      double _a;
      double _b;

    public:
      Ellipse();
      Ellipse(const Point& center, const double& a, const double& b);

      void AddVertex(const Point& point) { _center = point; }
      void SetSemiAxisA(const double a) { _a = a; }
      void SetSemiAxisB(const double b) { _b = b; }
      double Perimeter() const;
  };

  class Circle : public Ellipse
  {
    public:
      Circle() : Ellipse() {}
      Circle(const Point& center, const double& radius);
  };


  class Triangle : public IPolygon
  {
    protected:
      vector<Point> points;

    public:
      Triangle() { points.reserve(3); }
      Triangle(const Point& p1, const Point& p2, const Point& p3);

      void AddVertex(const Point& point) { points.push_back(point); }
      double Perimeter() const;
  };


  class TriangleEquilateral : public Triangle
  {
    public:
      TriangleEquilateral() : Triangle() {}
      TriangleEquilateral(const Point& p1, const double& edge);
  };

  class Quadrilateral : public IPolygon
  {
    protected:
      vector<Point> points;

    public:
      Quadrilateral() { points.reserve(4); }
      Quadrilateral(const Point& p1, const Point& p2, const Point& p3, const Point& p4);

      void AddVertex(const Point& p) { points.push_back(p); }
      double Perimeter() const;
  };

  class Rectangle : public Quadrilateral
  {
    public:
      Rectangle() : Quadrilateral() {}
      Rectangle(const Point& p1, const Point& p2, const Point& p3, const Point& p4);
      Rectangle(const Point& p1, const double& base, const double& height);
  };

  class Square: public Rectangle
  {
    public:
      Square() : Rectangle() {}
      Square(const Point& p1, const Point& p2, const Point& p3, const Point& p4);
      Square(const Point& p1, const double& edge);
  };
}

#endif // SHAPE_H

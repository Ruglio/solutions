#include "Intersector2D1D.h"

// ***************************************************************************
Intersector2D1D::Intersector2D1D()
{
    toleranceIntersection = 1.0E-7;
    toleranceParallelism  = 1.0E-7;
}
// ***************************************************************************
Vector3d Intersector2D1D::IntersectionPoint()
{
    // there is no intersection or it hasn t been computed yet
    if (IntersectionType() != PointIntersection)
        throw runtime_error("There is no intersection");

    return *lineOriginPointer + intersectionParametricCoordinate*(*lineTangentPointer);
}
// ***************************************************************************
void Intersector2D1D::SetPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    planeNormalPointer = &planeNormal;
    planeTranslationPointer = &planeTranslation;
}
// ***************************************************************************
void Intersector2D1D::SetLine(const Vector3d& lineOrigin, const Vector3d& lineTangent)
{
    lineOriginPointer = &lineOrigin;
    lineTangentPointer = &lineTangent;
}
// ***************************************************************************
bool Intersector2D1D::ComputeIntersection()
{
    double normalDotTangent = (*planeNormalPointer).dot(*lineTangentPointer);
    double normalDotDifferencePlaneLine = *planeTranslationPointer - (*planeNormalPointer).dot(*lineOriginPointer);
    // check if plane normal n is perpendicular to tangent vector, this means line and plane are parallel
    if (-toleranceParallelism <= normalDotTangent && normalDotTangent <= toleranceParallelism)
    {
        // check if n.dot(point_plane - line_origin) = n.dot(point_plane) - n.dot(line_origin) = plane_traslation - n.dot(line_origin) = 0
        // this means the line is inside the plane
        if (-toleranceParallelism <= normalDotDifferencePlaneLine && normalDotDifferencePlaneLine <= toleranceParallelism)
        {
            intersectionType = Coplanar;
            return false;
        }
        else
        {
            intersectionType = NoInteresection;
            return false;
        }
    }

    intersectionType = PointIntersection;
    double s = normalDotDifferencePlaneLine / normalDotTangent;
    intersectionParametricCoordinate = s;
    return true;
}

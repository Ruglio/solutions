#include "Intersector2D2D.h"

// ***************************************************************************
Intersector2D2D::Intersector2D2D()
{
    toleranceIntersection = 1.0E-7;
    toleranceParallelism  = 1.0E-5;

    matrixNormalVector.setZero();
    rightHandSide.setZero();
}

// ***************************************************************************
void Intersector2D2D::SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNormalVector.row(0) = planeNormal;
    rightHandSide(0) = planeTranslation;
}

// ***************************************************************************
void Intersector2D2D::SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNormalVector.row(1) = planeNormal;
    rightHandSide(1) = planeTranslation;
}

// ***************************************************************************
bool Intersector2D2D::ComputeIntersection()
{
    tangentLine = (matrixNormalVector.row(0)).cross(matrixNormalVector.row(1));
    // check if N_1 is parallel to N_2, this would mean planes are parallel
    if (tangentLine.squaredNorm() <= toleranceParallelism * toleranceParallelism)
    { // parallel planes

        // check if are coincident, so if plane traslation have the same ratio of normal vectors
        // because parallel planes could be multiple
        double ratio = 0;
        if (matrixNormalVector(0, 0) >= 1.0E-14)        // it is not 0
            ratio = matrixNormalVector(0, 1) / matrixNormalVector(0, 0);
        else if (matrixNormalVector(0, 1) >= 1.0E-14)   // it is not 0
            ratio = matrixNormalVector(1, 1) / matrixNormalVector(0, 1);
        else
            ratio = matrixNormalVector(1, 2) / matrixNormalVector(0, 2);

        // beside the same multiples of the normal vectors plane traslation parametres are equal
        if (abs(rightHandSide(1) - rightHandSide(0) * ratio) <= 1.0E-16)
        {
            intersectionType = Coplanar;
            return false;
        }
        else
        {
            intersectionType = NoInteresection;
            return false;
        }
    }

    // solve the system to get a starting point
    pointLine = matrixNormalVector.colPivHouseholderQr().solve(rightHandSide);
    intersectionType = LineIntersection;
    return true;
}

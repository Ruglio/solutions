#ifndef INTERSECTOR2D2D_HPP
#define INTERSECTOR2D2D_HPP

#include "Eigen"
#include "iostream"

using namespace std;
using namespace Eigen;

class Intersector2D2D;

class Intersector2D2D
{
  public:
    enum TypeIntersection
    {
      NoInteresection = 0,
      Coplanar = 1,
      LineIntersection = 2
    };

  protected:
    TypeIntersection intersectionType;

    double toleranceParallelism;
    double toleranceIntersection;

    // parameters to identify the intersection line
    Vector3d pointLine;
    Vector3d tangentLine;

    // rightHand is a vector containing plane traslation parameter
    Vector3d rightHandSide;
    // matrix where first and second rows are the normal vectors to the planes (N1, N2)
    // while the third row has N1 x N2 which is the tangent vector of the line
    Matrix3d matrixNormalVector;

  public:
    Intersector2D2D();

    void SetTolleranceIntersection(const double& _tolerance) { toleranceIntersection = _tolerance; }
    void SetTolleranceParallelism(const double& _tolerance) { toleranceParallelism = _tolerance; }

    const TypeIntersection& IntersectionType() const { return intersectionType; }
    const double& ToleranceIntersection() const { return toleranceIntersection; }
    const double& ToleranceParallelism() const { return toleranceParallelism; }
    const Vector3d& PointLine() const { return pointLine; }
    const Vector3d& TangentLine() const { return tangentLine; }

    void SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation);
    void SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation);
    bool ComputeIntersection();
};

#endif //INTERSECTOR2D2D_HPP

